*******
Install
*******

Requirements
============

* LXD

or

* podman

Install
=======

No additional python packages required.  Apt installs necessary requirements
while installing `lxd`.

or

.. code-block:: bash

  $ sudo pip install molecule molecule-podman
