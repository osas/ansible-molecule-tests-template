# Ansible Tests using Molecule: Template Configuration

In OSPO we use Molecule to tests our Ansible roles. This is our common
configuration and reference.

The goal is to leverage the flexibility of both Molecule and Ansible to
provide a base configuration which can be extended in seprates files.
This way upgrading the configuration should simply be done by
overwriting these base files and adapting the rest of the rules
according to the upgrades detailed in the changelog file.

This template is made for Molecule v3. We use Testinfra for testing the
deployment.

## Network Configuration

### Network Layout

Many roles require multiple containers to be tested properly, either to
test communication with another service, check firewall rules, or ACLs.
Because of this this template defines a separate network where
containers may communicate with each other and also fetch outside
resources (installed packages for example). On Docker this is done by
using a separate bridge network, on LXD the default network is fine.

### Containers Inter-Communication

On Docker you need to specify for each container which ports can be
accessed from other containers on the network using the `exposed_ports`
parameter. On LXD this is not needed.

### IPs

IPv4 and IPv6 are automatically assigned for each container. Docker's
network is setup to enable IPv6 in this template, LXD already has a
working default network.

You can discover the assigned IPs using Ansible facts
(`ansible_default_ipv4` and `ansible_default_ipv6`), so no need to
hardcode anything in your tests.

### Hostnames

Container's hostname is set using the Molecule's platform name and the
`example.com` domain.

Resolution of DNS names is not garanteed to work except the container's
own name. Anyway to ensure your tests work both on IPv4 and IPv6 it is
recommended to use IPs directly (found in Ansible facts).

### Firewall

Our roles often manage their own firewall permissions using Firewalld,
so it is installed on RedHat systemd because on this OS this is the
default solution. You can disable this feature by setting
`manage_firewall` to False; several of our roles support this variable
already to disengage the firewalling support, so this seemed
appropriate.

## Description of the Template

The `role` subdirectory contains the configuration to be merged-in.

On top you'll find the configuration for the various linters.

Then the `molecule` subdirectory with a `base configuration` (to be used
with the molecule's --base-config option).

The `ospo_template_version` file contains the version of this template
to remember which version was used and ease migration.

In the `_resources` with share various resources between the scenarios:

- **{create|destroy}-{podman|lxd}.yml**: playbooks to setup containers
  and network using a merged config using group_vars and host_vars
  (as is usual with Ansible)
- **Dockerfile.j2**: for Docker (layer on top of any OS image)
- **group_vars/all/common.yml**: common molecule drivers config
- **group_vars/all/firewall_workaround.yml**: workaround for a bug in
  Docker with cgroups
- **prepare.yml**: second-step setup of the containers using Ansible
  (LXD uses Ansible/raw to get the basics to be able to run Ansible
  fully, and Docker uses its Dockerfile in the previous stage, not
  the most convenient to modify, so the rest is here)

## CI Configuration

We provide configuration for:

- **GitLab CI**: .gitlab-ci.yml
- **Shippable**: shippable.yml (initially to use on GitHub but needs
  to be refreshed or replaced)

### Centralized Rules

In order to provide fixes in a centralized way without having to upgrade each and every role individually, it is possible to instead include this template remotely.

This mode is only available for GitLab CI.

In this mode you do no need to add the template files to your role, but instead use this `.gitlab-ci.yml`:

```
---

# it is not possible to use custom variables in includes, so it has to match
variables:
  TEST_BRANCH: master

include:
  - https://gitlab.com/osas/ansible-molecule-tests-template/-/raw/master/.gitlab-ci.yml
```

The include will automatically fetch the template files and merge them before running the tests.

You may wish to stick to a specific version or branch, for testing, in case of bugs, or if that's your workflow, and you can do that by adjusting the `TEST_BRANCH` variable and the include URL.

## Template Usage

Copy a CI configuration depending on your hosting.

Copy the content of the `role` subdirectory into your role directory.

Around this template you need to define:

- **molecule/\_resources/prepare_custom.yml**: to add more preparation
  steps (since there is no way yet to conditionally include
  playbooks, this file need to be present and contain a proper YAML
  header and empty list of plays: "--- []").
- **molecule/\_resources/converge.yml**: your role deployment rules
- **molecule/<scenario>**: each of your scenarios, more details below
- **molecule/\_resources/tests/**: your common tests, more details
  below

You can also optionally add:

- **molecule/\_resources/host_vars/**: host_vars for your Ansible play
  as usual

## Scenario Definition

We define the containers needed for this specific scenario:

```
---
scenario:
  name: myscenario
platforms:
  - name: ansible-test-host1
    # LXD
    source:
      alias: 'centos/7/amd64'
    # Docker
    image: 'centos:7'
  - name: ansible-test-host2
    # LXD
    source:
      alias: 'debian/buster/amd64'
    # Docker
    image: 'debian:10'
provisioner:
  inventory:
    host_vars:
      ansible-test-host1:
        custom_var: 42
```

Specific configuration for the deployment is done using `group_vars`
and `host_vars`. If you need to have different setting depending on the
scenario, you can simply add `host_vars` in the scenario definition like
the above example, so for more complex cases use conditionals in the
converge playbook.

Tests lives in the `tests` subdirectory. Tests shared among various
scenarios should be stored in `molecule/_resources/tests/` instead and
symlinked in the `tests` subdirectory.

## Problems

### Docker and Firewalld

In this configuration we have found problems accessing the network, not
just intercommunication but access to the outside to install new
packages. because of this the `manage_firewall` variable is
automatically set to False as a workaround.

LXD is not affected. Other firewalling solution were not tested.

TODO: affects podman???
